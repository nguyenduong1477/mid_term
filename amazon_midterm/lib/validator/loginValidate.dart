mixin LoginValidator {
  String? validateEmail(String? email) {
    if (email!.isEmpty) {
      return 'Email address is required.';
    }

    final regex = RegExp('[^@]+@[^\.]+\..+');
    if (!regex.hasMatch(email)) {
      return 'Enter a valid email';
    }

    return null;
  }

  String? validatePassword(String? password) {
    RegExp upper = RegExp(r'^(?=.*[A-Z])');
    RegExp lower = RegExp(r'^(?=.*[a-z])');
    RegExp special_character = RegExp(r'^(?=.*?[!@#\$&*~.])');
    if (password!.length < 4) {
      return 'Length of password must be above 4.';
    }
    else if (!upper.hasMatch(password))
    {
      return "at least 1 capital";
    }

    else if(!lower.hasMatch(password))
    {
      return "at least 1 lowercase";
    }

    else if(!special_character.hasMatch(password))
    {
      return "at least 1 special character";
    }
  }
}
