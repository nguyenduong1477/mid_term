class Product {
  late String imageUrl;
  late String name;
  late double price;
  late String description;

  Product({
    required this.imageUrl,
    required this.name,
    required this.price,
    required this.description,
  });
}

final List<Product> products = [
  Product(
    imageUrl: 'assets/image/goldwatch.jpg',
    name: 'Gold Plated Watch',
    price: 38.93,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/camera.jpg',
    name: 'Panasonic Camera',
    price: 1797.99,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/router.jpg',
    name: 'TP-Link Wifi Router',
    price: 53.99,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/bike.jpg',
    name: 'Schwinn Toddler Bike',
    price: 90.82,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/hat.jpg',
    name: 'Yellow Basball Cap',
    price: 11.89,
    description:
    '',
  ),
];

final List<Product> books = [
  Product(
    imageUrl: 'assets/image/lockeddoor.jpg',
    name: 'The Locked Door',
    price: 3.49,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/jack.jpg',
    name: 'Better Off Dead',
    price: 9.99,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/regret.jpg',
    name: 'Regretting You',
    price: 5.99,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/quiet.jpg',
    name: 'At the Quiet Edge: A Novel',
    price: 4.99,
    description:
    '',
  ),
];

final List<Product> cart = [
  products[3],
  books[2],
  products[1],
  books[0],
  products[4],
];
