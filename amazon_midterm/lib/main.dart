import 'package:amazon_midterm/ui/homeScreen.dart';
import 'package:flutter/material.dart';
import '../ui/loginScreen.dart';
import '../ui/homeScreen.dart';
import '../ui/cartScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Amazon',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryIconTheme: Theme.of(context).primaryIconTheme.copyWith(
          color: Colors.yellow,
        ),
      ),
      home: LoginScreen(),
    );
  }
}