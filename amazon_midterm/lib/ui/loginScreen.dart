import 'package:flutter/material.dart';
import '../validator/loginValidate.dart';
import '../ui/homeScreen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key}) : super(key: key);
  static const route = '/login';

  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }

}

class LoginScreenState extends State<StatefulWidget> with LoginValidator {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Login into Amazon'),
          backgroundColor: Colors.orange),
      body: buildLoginForm(),
    );
  }

  Widget buildLoginForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          Container(margin: EdgeInsets.only(top: 15.0),),
          Container(
            width: 300.0,
            child: emailField(),
          ),
          Container(margin: EdgeInsets.only(top: 8.0),),
          Container(
              width: 300.0,
              child: passwordField()
          ),
          Container(margin: EdgeInsets.only(top: 22.0),),
          Container(
            width: 100.0,
            height: 50.0,
            child: loginButton(),
          ),
        ],
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      controller: emailController,
      decoration: InputDecoration(
          enabledBorder:OutlineInputBorder(
            borderSide: BorderSide (color: Colors.orange),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide (color: Colors.orange),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide (color: Colors.red),
          ),
          focusedErrorBorder:OutlineInputBorder(
            borderSide: BorderSide (color: Colors.red),
          ) ,
          icon: Icon(Icons.person,color: Colors.orange,),
          focusColor: Colors.orange,
          labelText: 'Email',
          labelStyle: new TextStyle(
              color: Colors.orange
          )
      ),
      validator: validateEmail,
    );
  }

  Widget passwordField() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: InputDecoration(
          enabledBorder:OutlineInputBorder(
            borderSide: BorderSide (color: Colors.orange),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide (color: Colors.orange),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide (color: Colors.red),
          ),
          focusedErrorBorder:OutlineInputBorder(
            borderSide: BorderSide (color: Colors.red),
          ) ,
          icon: Icon(Icons.password, color: Colors.orange,),
          labelText: 'Password',
          labelStyle: new TextStyle(
              color: Colors.orange
          )
      ),
      validator: validatePassword,
    );
  }

  Widget loginButton() {
    return ElevatedButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(Colors.orange),
      ),
      onPressed: validate,
      child: Text('Login'),
    );

  }

  void validate() {
    final form = formKey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final email = emailController.text;
      final password = passwordController.text;

      Navigator.push(context,MaterialPageRoute(builder: (_)=>HomeScreen(key: null,)));
    }
  }
}
